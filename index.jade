mixin content
  section.title.inverse(data-background='#16a086')
    h1 Concurrency in Go
    h2 Paul Nechifor
    p.main-links
      a(href='/') Home
      = ' • '
      a(href='/blog') Blog
      = ' • '
      a(href='/projects') Projects
      = ' • '
      a(href='https://twitter.com/PaulNechifor') Twitter
    .sub-info
      p.left
        a(href='http://www.infoiasi.ro/bin/Main/') Faculty of Computer Science
      p.right 7 May 2014
  section
    h2 Outline
    ul
      li description of Go
      li concurrency approach
      li concurrency language features
      li concurrency patterns
      li conclusion and summary
  section
    h2 About Go
    ul
      li released in 2009, version 1.0 in 2012
      li a <strong>"server software"</strong> language
      li is <strong>compiled</strong> (not interpreted, not JITted) for multiple
        | architectures and OSes
      li only <strong>garbage collected</strong>
      li has lambdas, closures
  section
    h2 Syntax
    ul
      li similar to C, but cleaner
      li less verbosity (fewer parenthesis and mostly no semicolons)
      li uses <code>:=</code> for assignment and type inference
      li only looping construct is <code>for</code> (includes range loops)
      li generally safer constructs (e.g. no <code>break</code> in
        | <code>switch</code>)
  section
    h2 Concurrency Approach
    ul
      li <strong>concurrency</strong>: computations executing in overlapping
        | time periods
      li <strong>parallelism</strong>: use of multiple execution units
      li <strong>coroutines</strong>: generalization of subroutines with
        | multiple points for suspending and resuming execution
      li Go uses both for concurrency.
  section
    ul
      li Inspired by Hoare's Communicating Sequential Processes (CSP).
      li "Erlang takes some syntax from the CSP model, but with different
        | semantics."
      li "Go takes the semantics but not the syntax."
  section
    h2 Goroutines
    ul
      li light-weight processes (not coroutines)
      li multiplexed over native threads
      li has own call stack (heap allocated, fragmented, extensible)
      li 100000s are practical
  section
    h2 Comparison
    ul
      li Unlike Node.js, Python*, and Ruby*, Go is fully parallel.
    p *Have native threads but use a global interpreter lock.
  section
    pre
      code.big-pre.go.
        package main
        import (
          "fmt"
          "time"
        )
        func hello(s string) {
          for {
            fmt.Println("hello", s)
            time.Sleep(time.Second / 10)
          }
        }
        func main() {
          go hello("world")
          time.Sleep(time.Second)
        }
  section
    ul
      li Prints <code>"hello world"</code> about 10 times.
      li Program exits when <code>main</code> finishes.
      li This is unlike Java (where all non-daemon threads must end for the
        | program to end), or JavaScript (where the implicit event loop must be
        | empty).
  section
    h2 Channels
    ul
      li In the original CSP (as in Erlang) processes communicate by name.
      li In Go, goroutines communicate through unnamed channels.
      li Sending and receiving from channels are blocking operations (for the
        | goroutines).
      li This ties together communication and synchronization in a natural way.
      li Channels can be closed, but it is often unnecessary to do do so.
  section
    p Basic channel communication:
    pre
      code.big-pre.go.
        func other(c chan string) {
          c <- "Hello."
        }
        func main() {
          c := make(chan string)
          go other(c)
          value := <-c
          fmt.Println(value)
        }
  section
    pre
      code.big-pre.go.
        func main() {
          c := make(chan string)
          c <- "Hello." // Deadlock here.
          value := <-c
          fmt.Println(value)
        }
    ul
      li If the same code is written in a single function, Go detects the
        | deadlock at runtime.
    pre
      code.big-pre.no-highlight.
        throw: all goroutines are asleep - deadlock!
  section
    h2 Buffered Channels
    ul
      li Channels are unbuffered/blocking/synchronous by default.
      li Buffered channels are created by specifying the number of elements to
        | hold before blocking in <code>make</code>.
    pre
      code.big-pre.go.
        c := make(chan string, 10)
  section
    h2 Daisy Chaining Goroutines with Channels
    pre
      code.big-pre.go.
        func pass(from, to chan int) {
          to <- 1 + <-from
        }
        func main() {
          length := 1000000
          var start = make(chan int)
          var a = start
          var b chan int
          for i := 0; i < length; i++ {
            b = make(chan int)
            go pass(a, b)
            a = b
          }
          start <- 0
          fmt.Println(<-b)
        }
  section
    ul
      li 1000000 started goroutines (+1 for main)
      li 1000001 opened channels
      li on my laptop it take 2 seconds
      li goroutines are cheap!
    pre
      code.big-pre.no-highlight.
        time ./daisy-chain
        1000000
        real	0m2.225s
        user	0m1.232s
        sys	0m0.980s
  section
    h2 Multiple Receivers
    ul
      li It's trivial to implement a thread pool pattern.
    pre
      code.big-pre.go.
        func worker(id int, c chan string) {
          for {
            fmt.Println("worker", id, <-c)
          }
        }
        func main() {
          c := make(chan string)
          go worker(0, c)
          go worker(1, c)
          for i := 0; i < 10; i++ {
            c <- fmt.Sprintf("message %d", i)
          }
        }
  section
    pre
      code.big-pre.no-highlight.
        worker 0 message 0
        worker 0 message 2
        worker 1 message 1
        worker 0 message 3
        worker 0 message 5
        worker 1 message 4
        worker 0 message 6
        worker 1 message 7
        worker 0 message 8
  section
    h2 Select
    ul
      li Similar to <code>switch</code> statement, except cases refer to receive
        | or send operations.
      li Execution blocks until one operation can be performed.
    pre
      code.big-pre.go.
        select {
        case v := <-c1:
          fmt.Println("received:", v);
        case c2 <- "hello":
          fmt.Println("sent hello to c2");
        case c3 <- 55:
          fmt.Println("sent 55 to c3");
        }
    ul
      li If a default case exists, it is executed and the select is not blocked.
  section
    p Example pseudorandom send:
    pre
      code.big-pre.go.
        func send(c chan int) {
          for {
            select {
              case c <- 0:
              case c <- 1:
            }
          }
        }
        func main() {
          c := make(chan int)
          go send(c)
          for i := 0; i < 40; i++ {
            fmt.Printf("%d", <-c)
          }
        }
  section
    ul
      li When there are multiple possible operations, one is chosen
        | pseudorandomly.
    pre
      code.big-pre.no-highlight 1110100000001011011000010100101000101110
  section
    h2 Patterns
    ul
      li generator
      li quit channel
      li error handling
  section
    h2 Generator
    ul
      li Generators usually are simpler coroutines.
      li In Go, the generator pattern can be used to produce computationally
        | expensive results.
      li The pattern function makes a channel, launches a goroutine with the
        | channel as a closure and returns the channel.
  section
    pre
      code.big-pre.go.
        func gen(total int) chan string {
          c := make(chan string)
          go func() {
            for i := 0; i < total; i++ {
              c <- fmt.Sprintf("result %d", i)
            }
            close(c)
          }()
          return c
        }
        func main() {
          c := gen(10)
          for {
            if r, ok := <-c; ok {
              fmt.Println(r)
            } else {
              return
            }
          }
        }
  section
    p Simplified by using the range clause for channels.
    pre
      code.big-pre.go.
        func gen(total int) chan string {
          c := make(chan string)
          go func() {
            for i := 0; i < total; i++ {
              c <- fmt.Sprintf("result %d", i)
            }
            close(c)
          }()
          return c
        }
        func main() {
          for r := range gen(10) {
            fmt.Println(r)
          }
        }
  section
    h2 Quit Channel
    pre
      code.big-pre.go.
        func fn(msg string) (out chan string, quit chan bool) {
          out, quit = make(chan string), make(chan bool)
          go func() {
            for i := 0; ; i++ {
              time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
              select {
              case out <- fmt.Sprintf("%s %d", msg, i):
              case <-quit: return
              }
            }
          }()
          return
        }
  section
    pre
      code.big-pre.go.
        func main() {
          result, quit := fn("result")
          for {
            fmt.Println(<-result)
            if rand.Intn(2) == 0 {
              quit <- true
              break
            }
          }
        }
  section
    h2 Conclusions
    p Advantages:
    ul
      li Go's model is simpler than dealing with threads, locks, semaphores,
        | and the rest
      li more intuitive than dealing with callbacks as in Node.js
      li memory allocation can be faster since structures can be embedded in a
        | single zone (one allocation call) unlike Java and others
      li can use parallelism effectively
      li quick compilation, fast startup
  section
    p Disadvantages:
    ul
      li lack of exceptions can make it more verbose and repetitive
      li lack of generics makes it less type-safe
      li lack of JITting can make it slower than Java
  section
    h2 References
    - var links = []; // No multiline array support in Jade.
    - links.push('golang.org/ref/spec');
    - links.push('golang.org/doc/effective_go.html');
    - links.push('talks.golang.org/2012/concurrency.slide');
    - links.push('en.wikipedia.org/wiki/Go_(programming_language)');
    - links.push('en.wikipedia.org/wiki/Light-weight_process');
    - links.push('www.informit.com/articles/article.aspx?p=1768317');
    - links.push('www.golangpatterns.info/concurrency');
    - links.push('cxwangyi.wordpress.com/2012/07/29/chinese-whispers-in-racket-and-go/');
    ul
      each link in links
        li
          a(href='http://' + link)= link
  section.title.inverse(data-background='#16a086')
    h1 The End
    h2 Questions?

doctype html
html(lang='en')
  head
    meta(charset='utf-8')
    title Concurrency in Go
    meta(http-equiv='x-ua-compatible', content='ie=edge')
    meta(name='description', content='My presentation about Go concurrency.')
    link(rel='apple-touch-icon', href='/favicon152.png')
    meta(name='apple-mobile-web-app-capable', content='yes')
    meta(name='apple-mobile-web-app-status-bar-style', content='black-translucent')
    meta(name='viewport', content='width=device-width,initial-scale=1')
    link(rel='stylesheet', href='reveal/reveal.min.css')
    link(rel='stylesheet', href='style.css')
  body
    .reveal
      .slides
        +content()
    script(src='reveal/head.min.js')
    script(src='reveal/reveal.min.js')
    script(src='js/highlight.js')
    script(src='script.js')
