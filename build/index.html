<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>Concurrency in Go</title><meta http-equiv="x-ua-compatible" content="ie=edge"><meta name="description" content="My presentation about Go concurrency."><link rel="apple-touch-icon" href="/favicon152.png"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><meta name="viewport" content="width=device-width,initial-scale=1"><link rel="stylesheet" href="reveal/reveal.min.css"><link rel="stylesheet" href="style.css"></head><body><div class="reveal"><div class="slides"><section data-background="#16a086" class="title inverse"><h1>Concurrency in Go</h1><h2>Paul Nechifor</h2><p class="main-links"><a href="/">Home</a> • <a href="/blog">Blog</a> • <a href="/projects">Projects</a> • <a href="https://twitter.com/PaulNechifor">Twitter</a></p><div class="sub-info"><p class="left"><a href="http://www.infoiasi.ro/bin/Main/">Faculty of Computer Science</a></p><p class="right">7 May 2014</p></div></section><section><h2>Outline</h2><ul><li>description of Go</li><li>concurrency approach</li><li>concurrency language features</li><li>concurrency patterns</li><li>conclusion and summary</li></ul></section><section><h2>About Go</h2><ul><li>released in 2009, version 1.0 in 2012</li><li>a <strong>"server software"</strong> language</li><li>is <strong>compiled</strong> (not interpreted, not JITted) for multiple
architectures and OSes</li><li>only <strong>garbage collected</strong></li><li>has lambdas, closures</li></ul></section><section><h2>Syntax</h2><ul><li>similar to C, but cleaner</li><li>less verbosity (fewer parenthesis and mostly no semicolons)</li><li>uses <code>:=</code> for assignment and type inference</li><li>only looping construct is <code>for</code> (includes range loops)</li><li>generally safer constructs (e.g. no <code>break</code> in
<code>switch</code>)</li></ul></section><section><h2>Concurrency Approach</h2><ul><li><strong>concurrency</strong>: computations executing in overlapping
time periods</li><li><strong>parallelism</strong>: use of multiple execution units</li><li><strong>coroutines</strong>: generalization of subroutines with
multiple points for suspending and resuming execution</li><li>Go uses both for concurrency.</li></ul></section><section><ul><li>Inspired by Hoare's Communicating Sequential Processes (CSP).</li><li>"Erlang takes some syntax from the CSP model, but with different
semantics."</li><li>"Go takes the semantics but not the syntax."</li></ul></section><section><h2>Goroutines</h2><ul><li>light-weight processes (not coroutines)</li><li>multiplexed over native threads</li><li>has own call stack (heap allocated, fragmented, extensible)</li><li>100000s are practical</li></ul></section><section><h2>Comparison</h2><ul><li>Unlike Node.js, Python*, and Ruby*, Go is fully parallel.</li></ul><p>*Have native threads but use a global interpreter lock.</p></section><section><pre><code class="big-pre go">package main
import (
  "fmt"
  "time"
)
func hello(s string) {
  for {
    fmt.Println("hello", s)
    time.Sleep(time.Second / 10)
  }
}
func main() {
  go hello("world")
  time.Sleep(time.Second)
}</code></pre></section><section><ul><li>Prints <code>"hello world"</code> about 10 times.</li><li>Program exits when <code>main</code> finishes.</li><li>This is unlike Java (where all non-daemon threads must end for the
program to end), or JavaScript (where the implicit event loop must be
empty).</li></ul></section><section><h2>Channels</h2><ul><li>In the original CSP (as in Erlang) processes communicate by name.</li><li>In Go, goroutines communicate through unnamed channels.</li><li>Sending and receiving from channels are blocking operations (for the
goroutines).</li><li>This ties together communication and synchronization in a natural way.</li><li>Channels can be closed, but it is often unnecessary to do do so.</li></ul></section><section><p>Basic channel communication:</p><pre><code class="big-pre go">func other(c chan string) {
  c <- "Hello."
}
func main() {
  c := make(chan string)
  go other(c)
  value := <-c
  fmt.Println(value)
}</code></pre></section><section><pre><code class="big-pre go">func main() {
  c := make(chan string)
  c <- "Hello." // Deadlock here.
  value := <-c
  fmt.Println(value)
}</code></pre><ul><li>If the same code is written in a single function, Go detects the
deadlock at runtime.</li></ul><pre><code class="big-pre no-highlight">throw: all goroutines are asleep - deadlock!</code></pre></section><section><h2>Buffered Channels</h2><ul><li>Channels are unbuffered/blocking/synchronous by default.</li><li>Buffered channels are created by specifying the number of elements to
hold before blocking in <code>make</code>.</li></ul><pre><code class="big-pre go">c := make(chan string, 10)</code></pre></section><section><h2>Daisy Chaining Goroutines with Channels</h2><pre><code class="big-pre go">func pass(from, to chan int) {
  to <- 1 + <-from
}
func main() {
  length := 1000000
  var start = make(chan int)
  var a = start
  var b chan int
  for i := 0; i < length; i++ {
    b = make(chan int)
    go pass(a, b)
    a = b
  }
  start <- 0
  fmt.Println(<-b)
}</code></pre></section><section><ul><li>1000000 started goroutines (+1 for main)</li><li>1000001 opened channels</li><li>on my laptop it take 2 seconds</li><li>goroutines are cheap!</li></ul><pre><code class="big-pre no-highlight">time ./daisy-chain
1000000
real	0m2.225s
user	0m1.232s
sys	0m0.980s</code></pre></section><section><h2>Multiple Receivers</h2><ul><li>It's trivial to implement a thread pool pattern.</li></ul><pre><code class="big-pre go">func worker(id int, c chan string) {
  for {
    fmt.Println("worker", id, <-c)
  }
}
func main() {
  c := make(chan string)
  go worker(0, c)
  go worker(1, c)
  for i := 0; i < 10; i++ {
    c <- fmt.Sprintf("message %d", i)
  }
}</code></pre></section><section><pre><code class="big-pre no-highlight">worker 0 message 0
worker 0 message 2
worker 1 message 1
worker 0 message 3
worker 0 message 5
worker 1 message 4
worker 0 message 6
worker 1 message 7
worker 0 message 8</code></pre></section><section><h2>Select</h2><ul><li>Similar to <code>switch</code> statement, except cases refer to receive
or send operations.</li><li>Execution blocks until one operation can be performed.</li></ul><pre><code class="big-pre go">select {
case v := <-c1:
  fmt.Println("received:", v);
case c2 <- "hello":
  fmt.Println("sent hello to c2");
case c3 <- 55:
  fmt.Println("sent 55 to c3");
}</code></pre><ul><li>If a default case exists, it is executed and the select is not blocked.</li></ul></section><section><p>Example pseudorandom send:</p><pre><code class="big-pre go">func send(c chan int) {
  for {
    select {
      case c <- 0:
      case c <- 1:
    }
  }
}
func main() {
  c := make(chan int)
  go send(c)
  for i := 0; i < 40; i++ {
    fmt.Printf("%d", <-c)
  }
}</code></pre></section><section><ul><li>When there are multiple possible operations, one is chosen
pseudorandomly.</li></ul><pre><code class="big-pre no-highlight">1110100000001011011000010100101000101110</code></pre></section><section><h2>Patterns</h2><ul><li>generator</li><li>quit channel</li><li>error handling</li></ul></section><section><h2>Generator</h2><ul><li>Generators usually are simpler coroutines.</li><li>In Go, the generator pattern can be used to produce computationally
expensive results.</li><li>The pattern function makes a channel, launches a goroutine with the
channel as a closure and returns the channel.</li></ul></section><section><pre><code class="big-pre go">func gen(total int) chan string {
  c := make(chan string)
  go func() {
    for i := 0; i < total; i++ {
      c <- fmt.Sprintf("result %d", i)
    }
    close(c)
  }()
  return c
}
func main() {
  c := gen(10)
  for {
    if r, ok := <-c; ok {
      fmt.Println(r)
    } else {
      return
    }
  }
}</code></pre></section><section><p>Simplified by using the range clause for channels.</p><pre><code class="big-pre go">func gen(total int) chan string {
  c := make(chan string)
  go func() {
    for i := 0; i < total; i++ {
      c <- fmt.Sprintf("result %d", i)
    }
    close(c)
  }()
  return c
}
func main() {
  for r := range gen(10) {
    fmt.Println(r)
  }
}</code></pre></section><section><h2>Quit Channel</h2><pre><code class="big-pre go">func fn(msg string) (out chan string, quit chan bool) {
  out, quit = make(chan string), make(chan bool)
  go func() {
    for i := 0; ; i++ {
      time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
      select {
      case out <- fmt.Sprintf("%s %d", msg, i):
      case <-quit: return
      }
    }
  }()
  return
}</code></pre></section><section><pre><code class="big-pre go">func main() {
  result, quit := fn("result")
  for {
    fmt.Println(<-result)
    if rand.Intn(2) == 0 {
      quit <- true
      break
    }
  }
}</code></pre></section><section><h2>Conclusions</h2><p>Advantages:</p><ul><li>Go's model is simpler than dealing with threads, locks, semaphores,
and the rest</li><li>more intuitive than dealing with callbacks as in Node.js</li><li>memory allocation can be faster since structures can be embedded in a
single zone (one allocation call) unlike Java and others</li><li>can use parallelism effectively</li><li>quick compilation, fast startup</li></ul></section><section><p>Disadvantages:</p><ul><li>lack of exceptions can make it more verbose and repetitive</li><li>lack of generics makes it less type-safe</li><li>lack of JITting can make it slower than Java</li></ul></section><section><h2>References</h2><ul><li><a href="http://golang.org/ref/spec">golang.org/ref/spec</a></li><li><a href="http://golang.org/doc/effective_go.html">golang.org/doc/effective_go.html</a></li><li><a href="http://talks.golang.org/2012/concurrency.slide">talks.golang.org/2012/concurrency.slide</a></li><li><a href="http://en.wikipedia.org/wiki/Go_(programming_language)">en.wikipedia.org/wiki/Go_(programming_language)</a></li><li><a href="http://en.wikipedia.org/wiki/Light-weight_process">en.wikipedia.org/wiki/Light-weight_process</a></li><li><a href="http://www.informit.com/articles/article.aspx?p=1768317">www.informit.com/articles/article.aspx?p=1768317</a></li><li><a href="http://www.golangpatterns.info/concurrency">www.golangpatterns.info/concurrency</a></li><li><a href="http://cxwangyi.wordpress.com/2012/07/29/chinese-whispers-in-racket-and-go/">cxwangyi.wordpress.com/2012/07/29/chinese-whispers-in-racket-and-go/</a></li></ul></section><section data-background="#16a086" class="title inverse"><h1>The End</h1><h2>Questions?</h2></section></div></div><script src="reveal/head.min.js"></script><script src="reveal/reveal.min.js"></script><script src="js/highlight.js"></script><script src="script.js"></script></body></html>