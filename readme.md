# Concurrency in Go

My presentation about Go concurrency.

![Concurrency in Go presentation cover](screenshot.png)

## Build it

Get the dependencies:

    yarn

Build it:

    yarn build

## License

MIT
